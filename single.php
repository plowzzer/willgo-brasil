<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package onegate_theme
 */

get_header(); ?>

<div style="padding-top:90px">


	<div class="uk-container uk-container-center">

		<?php
		while ( have_posts() ) : the_post();

		the_title('<h2>','</h2>');
		the_content();

		endwhile; // End of the loop.
		?>


	</div><!-- #primary -->
</div>
<?php

get_footer();
