<?php
/*
 * template-ajax-results.php
 * This file should be created in the root of your theme directory
 */

if ( have_posts() ) :?>
  <table class="uk-table uk-table-striped">
    <caption>Todas as vagas disponiveis segundo a pesquisa.</caption>
    <thead>
        <tr>
            <th>Nome da Vaga</th>
            <th>Carreira</th>
            <th>Local</th>
        </tr>
    </thead>

    <tbody>
   <?while ( have_posts() ) : the_post();
   $post_type = get_post_type_object($post->post_type);
   ?>
   <tr>
     <td><a href="<?the_permalink();?>"><?the_title();?></a> <small>ID <?= $post->ID;?></small></td>
     <td>
       <?$term_list_careers = wp_get_post_terms($post->ID, 'careers', array("parent"=>0));?>
       <?foreach($term_list_careers as $term_single) {?>
         <a href="<?= esc_url( home_url( '/' ) ); ?>careers/<?=$term_single->slug;?>"><?=$term_single->name;?></a>
       <?}?>
     </td>

     <td>
       <?$term_list_place = wp_get_post_terms($post->ID, 'place', array("parent"=>0));?>
       <?foreach($term_list_place as $term_single) {?>
         <a href="<?= esc_url( home_url( '/' ) ); ?>place/<?=$term_single->slug;?>"><?=$term_single->name;?></a>
       <?}?>
     </td>
   </tr>

   <?php
 endwhile;?>
</tbody>
</table>

<?else :
   echo '<p>Desculpe não encontramos nenhuma vaga com essa pesquisa, tente novamente.</p>';
endif;
wp_reset_query();
?>
