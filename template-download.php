<?php
/**
*
*Template Name: Downloads
*Template texto: Pagina de Downloads
*
* @package one
*/

get_header(); ?>

<div class="downloads">
  <div class="uk-container uk-container-center">
    <h3><i class="fa fa-android"></i> Android</h3>
    <div class="card uk-grid">
      <div class="uk-width-1-2">
        <a href="https://play.google.com/store/apps/details?id=com.willgo&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank">
          <span class="name">Usuário</span>
          <img class="app" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apps/android-user.png" alt="" />
          <img class="ga" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/pt-br-play-badge.png" />
          <span class="info">Disponível</span>
        </a>
      </div>


      <div class="uk-width-1-2">
        <a href="https://play.google.com/store/apps/details?id=com.willgopartner&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank">
          <span class="name">Empreendedor</span>
          <img class="app" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apps/android-partner.png" alt="" />
          <img class="ga" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/pt-br-play-badge.png" />
          <span class="info">Disponível</span>
        </a>
      </div>
    </div>

    <h3><i class="fa fa-apple"></i> iOS</h3>
    <div class="card uk-grid">
      <div class="uk-width-1-2">
        <a href="https://itunes.apple.com/pt/app/willgo/id1110286950?mt=8" target="_blank">
          <span class="name">Usuário</span>
          <img class="app" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apps/android-user.png" alt="" />
          <img class="ga" alt="Get it on Google Play" src="https://linkmaker.itunes.apple.com/images/badges/en-us/badge_appstore-lrg.svg" />
          <span class="info">Disponível</span>
        </a>
      </div>


      <div class="uk-width-1-2">
        <a href="https://itunes.apple.com/pt/app/willgo-partner/id1110286956?mt=8" target="_blank">
          <span class="name">Empreendedor</span>
          <img class="app" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apps/android-partner.png" alt="" />
          <img class="ga" alt="Get it on Google Play" src="https://linkmaker.itunes.apple.com/images/badges/en-us/badge_appstore-lrg.svg" />
          <span class="info">Disponível</span>
        </a>
      </div>
    </div>

    <!-- <h3><i class="fa fa-windows"></i> Windows Phone</h3>
    <div class="card uk-grid">
      <div class="uk-width-1-2">
        <a href="#">
          <span class="name">Usuário</span>
          <img class="app" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apps/android-user.png" alt="" />
          <img class="ga" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/pt-br-play-badge.png" />
          <span class="info red">Ainda não Disponível</span>
        </a>
      </div>


      <div class="uk-width-1-2">
        <a href="#">
          <span class="name">Empreendedor</span>
          <img class="app" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apps/android-partner.png" alt="" />
          <img class="ga" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/pt-br-play-badge.png" />
          <span class="info red">Ainda não Disponível</span>
        </a>
      </div>
    </div> -->
  </div>


</div>


<?php get_footer(); ?>
