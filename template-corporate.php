<?php
/**
*
*Template Name: Corporate
*Template texto: Usar como pagina WillGo corporate
*
* @package one
*/

get_header(); ?>

<div class="corporate">
  <?$background = get_field('banner');?>
  <div class="banner" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
    <div class="content uk-hidden-small ">
      <?php the_field('texto_banner'); ?>
    </div>
  </div>
  <div class="content-out uk-hidden-medium uk-hidden-large">
    <?php the_field('texto_banner'); ?>
  </div>

  <!-- <div id="about" class="uk-container uk-container-center">
    <div class="uk-grid sobre">
      <div class="uk-width-1-2 uk-hidden-small" style="text-align:center;">
        <?$sobre = get_field('sobre_imagem');?>
        <img src="<?php echo $sobre['url']; ?>" alt="<?php echo $sobre['alt']; ?>" />
      </div>
      <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('sobre_texto'); ?>
      </div>
    </div>
  </div> -->

  <div id="como-funciona" class="uk-container uk-container-center">
    <h1>Como funciona?</h1>
    <ul class="uk-grid">
      <? if( have_rows('como_funciona') ):
        while ( have_rows('como_funciona') ) : the_row();?>
          <li class="uk-width-1-1 uk-width-medium-1-4">
            <?$image = get_sub_field('imagem');?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            <h3><?the_sub_field('titulo');?></h3>
            <?the_sub_field('texto');?>
          </li>
        <?endwhile;
      endif;?>
    </ul>
  </div>

  <?$background = get_field('despesas_imagem');?>
  <div id="despesas" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <h1>Reduza despesas de viagens de<br>seus funcionários em até 40%</h1>
      <ul class="uk-grid">
        <? if( have_rows('despesas_texto') ):
          while ( have_rows('despesas_texto') ) : the_row();?>
            <li class="uk-width-1-1 uk-width-medium-1-3">
              <h3><?the_sub_field('titulo');?></h3>
              <?the_sub_field('texto');?>
            </li>
          <?endwhile;
        endif;?>
      </ul>
    </div>
  </div>

  <div id="seguranca" class="uk-container uk-container-center">
    <h1>Padrão de Segurança <br> WillGo Corporate</h1>
    <p style="font-size: 130%;">Certificamos  que seus funcionários estarão confortáveis e seguros durante toda a viagem</p>
    <ul class="uk-grid">
      <? if( have_rows('seguranca') ):
        while ( have_rows('seguranca') ) : the_row();?>
          <li class="uk-width-1-1 uk-width-medium-1-4">
            <?$image = get_sub_field('imagem');?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            <h3><?the_sub_field('titulo');?></h3>
            <?the_sub_field('texto');?>
          </li>
        <?endwhile;
      endif;?>
    </ul>
  </div>


  <?$background = get_field('banner-corpore');?>
  <div id="banner-footer" style="background-image: url(<?php echo $background['url']; ?>);"></div>


  <div class="formulario" id="contact">
    <div class="uk-container uk-container-center">
        <?php $formulario = get_field('formulario_de_contato');
        echo do_shortcode($formulario);?>
      </div>
      <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div>

</div>


<?php get_footer(); ?>
