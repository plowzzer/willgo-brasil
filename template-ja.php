<?php
/**
*
*Template Name: Já
*Template texto: Usar como pagina WillGo Já
*
* @package one
*/

get_header(); ?>

<div class="ja">
  <div class="banner">
    <video autoplay id="bgvid" loop muted autoplay>
      <source src="<?=bloginfo('stylesheet_directory')?>/assets/videos/video3.mov" type="video/mp4">
    </video>
    <div class="content">
      <p>Quantas pessoas e empresas você conhece, que diariamente precisam de transporte de objeto, documentos e encomendas?<br><br>É simples, é rapído é pra Já, Chame Willgo Já</p>
      <a class="cadastro-btn" href="http://willgobrasil.com.br/novo/cadastro">Cadastre-se já</a>
    </div>
  </div>

  <section class="about" id="about">
    <img class="moto uk-hidden-small" src="<?=bloginfo('stylesheet_directory')?>/assets/img/about-moto.png" alt="" />
    <div class="uk-grid uk-hidden-small">
      <div class="uk-width-small-1-1 uk-width-medium-1-2 image-field">
        <?php $image = get_field('about_image');
        if( !empty($image) ): ?>
        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-1-2 text-field">
        <?php the_field('about_texto'); ?>
      </div>
    </div>
    <div class="uk-grid uk-visible-small">
      <div class="uk-width-small-1-1 uk-width-medium-1-2 text-field-small">
        <?php the_field('about_texto'); ?>
        <span><a class="cadastro-btn" href="http://willgobrasil.com.br/novo/cadastro">Cadastre-se Já</a></span>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-1-2 image-field-small">
        <?php $image = get_field('about_image');
        if( !empty($image) ): ?>
        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
    </div>
  </section>

  <section class="video" id="video">
      <?php $image = get_field('image_to_call'); ?>
      <a href="<?php the_field('video-embed'); ?>" data-uk-lightbox><img class="image-call" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
      <h1>Como funciona, assista o vídeo</h1>
  </section>

  <? $samedayBackground = get_field('willgo_sameday_background');?>
  <section class="same_day" id="sameday" style="background-image: url('<?=$samedayBackground[url];?>')">
    <?$samedayLogo = get_field('willgo_sameday_logo');?>
    <div class="uk-container uk-container-center">
      <div class="uk-text-center">
        <?php if ($samedayLogo): ?>
          <img src="<?=$samedayLogo['url'];?>" alt="WillGo Same Day">
        <?php endif; ?>
      </div>
      <?the_field('willgo_sameday_text');?>
      <?php if (get_field('willgo_sameday_url')): ?>
        <div class="uk-text-center">
          <a href="<?the_field('willgo_sameday_url');?>" class="btn">Cadastre já sua empresa</a>
        </div>
      <?php endif; ?>
    </div>
  </section>


  <section class="para_voce" id="paravoce">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-small-1-1 uk-width-medium-1-3 uk-hidden-small image-field1">
        <?php $image = get_field('para_voce_imagem_esquerda');
        if( !empty($image) ): ?>
        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-1-3 text-field">
        <?php the_field('para_voce_texto'); ?>
        <span><a class="cadastro-btn" href="http://willgobrasil.com.br/novo/cadastro">Cadastre-se Já</a></span>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-1-3 image-field2">
        <?php $image = get_field('para_voce_imagem_direita');
        if( !empty($image) ): ?>
        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
    </div>
  </section>

  <section class="para_suaempresa" id="paraempresa">
    <div class="uk-container uk-container-center">
      <h1>Para Sua Empresa</h1>
      <?php $image = get_field('para_empresa_storyboard');
      if( !empty($image) ): ?>
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
      <?php endif; ?>
      <img class="" src="<?=bloginfo('stylesheet_directory')?>/assets/img/storyboard-shadow.png" alt="" />
      <p style="text-align: center;">
        <span><a class="cadastro-btn" href="https://www.willgobrasil.com.br/novo/site-pack-empresa-cadastro_empresa">Cadastre sua empresa</a></span>
      </p>
    </div>
  </section>

  <section class="suas_compras" id="suascompras">
    <div class="uk-grid uk-grid-collapse">
      <div class="uk-width-small-1-1 uk-width-medium-1-4 uk-hidden-small image-field1">
        <?php $image = get_field('compras_imagem_esquerda');
        if( !empty($image) ): ?>
        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-2-4 text-field">
        <?php the_field('compras_texto'); ?>
        <p style="text-align: center;"><a class="cadastro-btn" href="http://willgobrasil.com.br/novo/cadastro">Cadastre-se Já</a></p>
      </div>
      <div class="uk-width-small-1-1 uk-width-medium-1-4 image-field2">
        <?php $image = get_field('compras_imagem_direita');
        if( !empty($image) ): ?>
        	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
      </div>
    </div>
  </section>

  <section class="ja-call" id="ja-call">
    <div class="uk-grid uk-grid-collapse">
      <div class="woman uk-width-1-2 uk-width-medium-1-2 uk-width-large-1-3 uk-hidden-small">
        <img class="back" src="<?=bloginfo('stylesheet_directory')?>/assets/img/bau.png" alt="" />
        <?php $image4 = get_field('ja-imagem-4');
        if( !empty($image4) ): ?>
          <img class="icone" src="<?php echo $image4['url']; ?>" alt="<?php echo $image4['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="text_place uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3">
        <h1>Para Seu Delivery</h1>
        <?php the_field('ja-texto'); ?>
        <span><a class="cadastro-btn" href="https://www.willgobrasil.com.br/novo/site-pack-empresa-cadastro_empresa">Cadastre sua empresa</a></span>
        <div class="icons">
          <?php
          $image1 = get_field('ja-imagem-1');
          $image2 = get_field('ja-imagem-2');
          $image3 = get_field('ja-imagem-3');
          if( !empty($image1) ): ?>
            <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
            <img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
            <img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
          <?php endif; ?>
          </div>
        </div>
        <div class="moto uk-width-1-3 uk-hidden-small uk-hidden-medium">
          <img class="back" src="<?=bloginfo('stylesheet_directory')?>/assets/img/mulher2.png" alt="" />
          <?php $image5 = get_field('ja-imagem-5');
          if( !empty($image5) ): ?>
            <img class="ico" src="<?php echo $image5['url']; ?>" alt="<?php echo $image5['alt']; ?>" />
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

</div>


<?php get_footer(); ?>
