<?php
/**
*
*Template Name: Embaixador
*Template texto: Usar como pagina WillGo Embaixador
*
* @package one
*/

get_header(); ?>

<div class="ambassador">
  <?$background = get_field('banner');?>
  <div  class="banner" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('texto_banner'); ?>
          <p style="text-align:center;">
            <a href="#contact" data-uk-smooth-scroll="{offset: 91}">Seja um embaixador WillGo</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <?$background = get_field('call1_image');?>
  <div id="faturar" class="call1" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-2">

        </div>
        <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('call1_texto'); ?>
        </div>
      </div>
    </div>
  </div>


  <?$background = get_field('call2_image');?>
  <div id="bonus" class="call2" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('call2_texto'); ?>
        </div>
      </div>
    </div>
  </div>

  <?$background = get_field('call3_image');?>
  <div id="exp" class="call1" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-2">

        </div>
        <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('call3_texto'); ?>
        </div>
      </div>
    </div>
  </div>

  <? if( have_rows('depoimentos') ) {?>
    <div id="depoimentos">
      <div class="uk-container uk-container-center">
        <h1>Depoimentos</h1>
        <div data-uk-slideset="{small: 1, medium: 3}">
          <div class="uk-slidenav-position">
            <ul class="uk-grid uk-slideset">
              <?while ( have_rows('depoimentos') ) : the_row() ;?>
                <li>
                  <p><?the_sub_field('depo');?></p>
                  <h4><?the_sub_field('name');?></h4>
                  <h4><?the_sub_field('fuction');?></h4>
                </li>
              <?endwhile;?>
            </ul>
            <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideset-item="previous"></a>
            <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideset-item="next"></a>
          </div>
          <ul class="uk-slideset-nav uk-dotnav uk-flex-center"></ul>
        </div>
      </div>
    </div>

  <?} //endif?>

  <? if( have_rows('questions') ) {?>
    <div id="respostas">
      <div class="uk-container uk-container-center">
        <h1>Perguntas e Respostas</h1>
        <ul class="uk-grid">
          <?while ( have_rows('questions') ) : the_row() ;?>
            <li class="uk-width-1-1">
              <h2><?the_sub_field('title');?></h2>
              <p><?the_sub_field('texto');?></p>
            </li>
          <?endwhile;?>
        </ul>

      </div>
    </div>

  <?} //endif?>

  <div class="formulario" id="contact">
    <div class="uk-container uk-container-center">
      <h1>Seja um Embaixador WillGo</h1>
        <?php $formulario = get_field('formulario_de_contato');
        echo do_shortcode($formulario);?>
      </div>
      <!-- <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div> -->

</div>
<p style="text-align:center;">
  <small>*em créditos utilizáveis dentro da plataforma WillGo.</small>
</p>


<?php get_footer(); ?>
