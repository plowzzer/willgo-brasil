<?php
/**
*
*Template Name: Assessoria de Imprensa
*Template texto: Usar como pagina WillGo Assessoria
*
* @package one
*/

get_header(); ?>

<div class="assessoria">
  <div class="uk-container uk-container-center into">
    <?php while ( have_posts() ) : the_post();
      the_content();
    endwhile; ?>


  </div>
  <div class="formulario">
    <div class="uk-container uk-container-center">
        <?php $formulario = get_field('formulario_de_contato');
        echo do_shortcode($formulario);?>
      </div>
      <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div>

</div>


<?php get_footer(); ?>
