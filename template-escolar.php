<?php
/**
*
*Template Name: Escolar
*Template texto: Usar como pagina WillGo Escolar
*
* @package one
*/

get_header(); ?>

<div class="escolar">
  <div class="banner">
    <? $banner_image = get_field('banner_image');?>
    <img src="<?= $banner_image['url'];?>" alt="">
    <div class="content">
      <?the_field('banner_content');?>
    </div>
  </div>

  <div class="call">
    <h2>Gaste seu tempo com sua família da melhor forma.</h2>
  </div>

  <div class="call2" id="team2">
    <? $banner_image2 = get_field('banner_image2');?>
    <img src="<?= $banner_image2['url'];?>" alt="">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <?the_field('call2_content');?>
          <p class="uk-text-center" style="margin-top: 30px; margin-bottom:30px;">
            <a class="btn" href="#">Contrate Já</a>
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="call3" id="solucao">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <?the_field('call3_content');?>
        </div>
      </div>
    </div>
  </div>

  <div class="about" id="sobre">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <?the_field('call_about');?>
        </div>
      </div>
    </div>
  </div>

  <? $background_mensalidade = get_field('mensal_bg');?>
  <div class="mensalidadecall" id="plano" style="background-image: url('<?=$background_mensalidade['url'];?>')">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-text-center">
          <h2>Ative seu Plano de Mensalidade</h2>
        </div>
        <div class="uk-width-1-2 uk-width-medium-1-3">
          <?the_field('mensal_content');?>
          <a class="btn" href="#">Cadastre-se Já</a>
        </div>
        <div class="uk-width-1-2 uk-width-medium-1-3">
          <div class="circle_value">
            <div class="content">
              <p><small>Por apenas R$</small></p>
              <h3><?the_field('mensalidade_price');?></h3>
              <p class="uk-text-right"><small>Mensal</small></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="treinamento" id="training">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1">
          <h2>Treinamento e Capacitação</h2>
        </div>
        <?php

        // check if the repeater field has rows of data
        if( have_rows('repeater_field_name') ):
            while ( have_rows('repeater_field_name') ) : the_row();?>
              <div class="uk-width-1-1 uk-width-medium-1-3 uk-text-center">
                <?$icon=get_sub_field('icon');?>
                <img src="<?=$icon['url'];?>" alt="">
                <?the_sub_field('content');?>
              </div>
            <?endwhile;
        else :?>
          <div class="uk-width-1-1">
            <p class="uk-text-center">Não foi encontrado nenhum treinamento no momento</p>
          </div>
        <?endif;

        ?>
      </div>
    </div>
  </div>

  <div class="formulario" id="cad">
    <div class="uk-container uk-container-center">
        <?php $form = get_field('formulario');
        echo do_shortcode($form);?>
      </div>
      <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div>

</div>


<?php get_footer(); ?>
