<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package onegate_theme
 */

get_header(); ?>

<div class="workatwillgo">
	<div class="banner">
		<?$banner = get_field('work_banner', 'options');?>
		<img src="<?= $banner['url'];?>" alt="">
		<div class="content">
			<?the_field('work_description', 'option');?>
    </div>
	</div>
		<div class="uk-container uk-container-center">

			<div class="description">
				<h1><?the_field('work_titlecall', 'option');?></h1>
				<h2><?the_field('work_subtitlecall', 'option');?></h2>
			</div>

			<h3>Carreiras</h3>
			<div class="uk-grid uk-grid-width-1-1 uk-grid-width-medium-1-3" data-uk-grid-match>
				<?$args = array(
					'taxonomy'               => array( 'careers' ),
					'orderby'                => 'name',
			    'order'                  => 'ASC',
			    'hide_empty'             => true,
					'parent'           			 => 0,
				);?>

				<?$careers = new WP_Term_Query($args);
				foreach($careers->get_terms() as $term){ ?>
					<div class="career">
						<?$image = get_field('career_picture', $term);?>
						<a href="<?php echo get_term_link( $term ); ?>"><img src="<?echo $image['url'];?>" alt="<?echo $image['alt'];?>" /></a>
						<h2 class="sub-title"><?echo $term->name;?></h2>
						<?if ($term->count == 1) {?>
							<h5>Temos <?=$term->count;?> vaga disponivel</h5>
						<?}else if($term->count >= 2){?>
							<h5>Temos <?=$term->count;?> vagas disponiveis</h5>
						<?}?>

						<?echo $term->description;?>
						<p class="absolute-padding" style="text-align:center;">
							<a class="btn" href="<?php echo get_term_link( $term ); ?>">Saiba Mais</a>
						</p>
					</div>
				<?}?>
			</div>

			<!-- <?php
			if ( have_posts() ) : ?>
				<h2>Todas as vagas</h2>
				<table class="uk-table uk-table-striped">
					<caption>...</caption>
					<thead>
							<tr>
									<th>Nome da Vaga</th>
									<th>Carreira</th>
									<th>Local</th>
							</tr>
					</thead>

					<tbody>


					<?php
					/* Start the Loop */
					$args_works = array( 'post_type' => array( 'work_at_willgo' ), 'posts_per_page' => -1 ,'orderby'=> 'title', 'order' => 'ASC' );
					$works_query = new WP_Query( $args_works );
					while ( $works_query->have_posts() ) : $works_query->the_post();?>

						<tr>
							<td><a href="<?the_permalink();?>"><?the_title();?></a> <small>ID <?= $post->ID;?></small></td>
							<td>
								<?$term_list_careers = wp_get_post_terms($post->ID, 'careers', array("parent"=>0));?>
								<?foreach($term_list_careers as $term_single) {?>
									<a href="<?= esc_url( home_url( '/' ) ); ?>careers/<?=$term_single->slug;?>"><?=$term_single->name;?></a>
								<?}?>
							</td>

							<td>
								<?$term_list_place = wp_get_post_terms($post->ID, 'place', array("parent"=>0));?>
								<?foreach($term_list_place as $term_single) {?>
									<a href="<?= esc_url( home_url( '/' ) ); ?>place/<?=$term_single->slug;?>"><?=$term_single->name;?></a>
								<?}?>
							</td>
						</tr>

					<?endwhile;?>

					</tbody>
				</table>


			<?	the_posts_navigation();

			else :

			//	echo "<p style='text-align:center;'>Não há nenhuma vaga no momento, volte mais tarde</p>";

			endif; ?> -->



			<?$search = new WP_Advanced_Search('work_search');?>
			<h3>Pesquisa por vagas:</h3>
			<div class="uk-grid">
			   <div id="search" class="uk-width-1-1 uk-width-medium-1-3 pesquisa">

			      <?php $search->the_form(); ?>
			   </div>

			   <div class="uk-width-1-1 uk-width-medium-2-3 resultados">
	         <div id="wpas-results"></div> <!-- This is where our results will be loaded -->
			   </div>
			</div>


	</div><!--container-->
</div><!--page-->

<?get_footer();
