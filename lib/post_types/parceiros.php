<? // Register Custom Post Type
function parceiros() {

	$labels = array(
		'name'                  => _x( 'Parceiros', 'Post Type General Name', 'parceiros' ),
		'singular_name'         => _x( 'Parceiro', 'Post Type Singular Name', 'parceiros' ),
		'menu_name'             => __( 'Parceiros', 'parceiros' ),
		'name_admin_bar'        => __( 'Parceiros', 'parceiros' ),
		'archives'              => __( 'Arquivo de parceiros', 'parceiros' ),
		'parent_item_colon'     => __( 'Parceiro pai:', 'parceiros' ),
		'all_items'             => __( 'Todos os itens', 'parceiros' ),
		'add_new_item'          => __( 'Adicionar novo parceiro', 'parceiros' ),
		'add_new'               => __( 'Adicionar novo', 'parceiros' ),
		'new_item'              => __( 'Novo parceiro', 'parceiros' ),
		'edit_item'             => __( 'Editar parceiro', 'parceiros' ),
		'update_item'           => __( 'Atualizar parceiro', 'parceiros' ),
		'view_item'             => __( 'Ver parceiro', 'parceiros' ),
		'search_items'          => __( 'Procurar por parceiro', 'parceiros' ),
		'not_found'             => __( 'Não encontrado', 'parceiros' ),
		'not_found_in_trash'    => __( 'Não encontrado no lixo', 'parceiros' ),
		'featured_image'        => __( 'Imagem de destaque', 'parceiros' ),
		'set_featured_image'    => __( 'Escolher como imagem de destaque', 'parceiros' ),
		'remove_featured_image' => __( 'Remover imagem destaque', 'parceiros' ),
		'use_featured_image'    => __( 'Usar como imagem de destaque', 'parceiros' ),
		'insert_into_item'      => __( 'Inserir dentro do parceiro', 'parceiros' ),
		'uploaded_to_this_item' => __( 'Atualizado para esse parceiro', 'parceiros' ),
		'items_list'            => __( 'Lista de parceiros', 'parceiros' ),
		'items_list_navigation' => __( 'Lista de navegação de parceiros', 'parceiros' ),
		'filter_items_list'     => __( 'Filtrar parceiros', 'parceiros' ),
	);
	$args = array(
		'label'                 => __( 'Parceiro', 'parceiros' ),
		'description'           => __( 'Parceiros do WillGo Brasil', 'parceiros' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
		'taxonomies'            => array( 'area_de_atuacao' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-star-filled',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'parceiros', $args );

}
add_action( 'init', 'parceiros', 0 );


// Register Custom Taxonomy
function area_de_atuacao() {

	$labels = array(
		'name'                       => _x( 'Áreas de Atuação', 'Taxonomy General Name', 'Taxonomia da Área de Atuação' ),
		'singular_name'              => _x( 'Área de Atuação', 'Taxonomy Singular Name', 'Taxonomia da Área de Atuação' ),
		'menu_name'                  => __( 'Área de Atuação', 'Taxonomia da Área de Atuação' ),
		'all_items'                  => __( 'Todos as áreas', 'Taxonomia da Área de Atuação' ),
		'parent_item'                => __( 'Área de atuação pai', 'Taxonomia da Área de Atuação' ),
		'parent_item_colon'          => __( 'Área parente:', 'Taxonomia da Área de Atuação' ),
		'new_item_name'              => __( 'Nova Área de Atuação', 'Taxonomia da Área de Atuação' ),
		'add_new_item'               => __( 'Adicionar nova Área', 'Taxonomia da Área de Atuação' ),
		'edit_item'                  => __( 'Editar área de atuação', 'Taxonomia da Área de Atuação' ),
		'update_item'                => __( 'Atualizar área de atuação', 'Taxonomia da Área de Atuação' ),
		'view_item'                  => __( 'Ver área de atuação', 'Taxonomia da Área de Atuação' ),
		'separate_items_with_commas' => __( 'Separar áreas por virgulas', 'Taxonomia da Área de Atuação' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover áreas', 'Taxonomia da Área de Atuação' ),
		'choose_from_most_used'      => __( 'Escolher entre os mais utilizados', 'Taxonomia da Área de Atuação' ),
		'popular_items'              => __( 'Áreas populares', 'Taxonomia da Área de Atuação' ),
		'search_items'               => __( 'Procurar por área', 'Taxonomia da Área de Atuação' ),
		'not_found'                  => __( 'Não encontrado', 'Taxonomia da Área de Atuação' ),
		'no_terms'                   => __( 'Nenhuma área', 'Taxonomia da Área de Atuação' ),
		'items_list'                 => __( 'Lista de áreas', 'Taxonomia da Área de Atuação' ),
		'items_list_navigation'      => __( 'Lista de áreas', 'Taxonomia da Área de Atuação' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'area_de_atuacao', array( 'parceiros' ), $args );

}
add_action( 'init', 'area_de_atuacao', 0 );?>
