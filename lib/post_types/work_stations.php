<?// Register Custom Post Type
function work_at_willgo() {

	$labels = array(
		'name'                  => _x( 'Vagas de trabalho', 'Post Type General Name', 'PostType to make work stations' ),
		'singular_name'         => _x( 'Vaga de Trabalho', 'Post Type Singular Name', 'PostType to make work stations' ),
		'menu_name'             => __( 'Vagas de trabalho', 'PostType to make work stations' ),
		'name_admin_bar'        => __( 'Vagas de trabalho', 'PostType to make work stations' ),
		'archives'              => __( 'Arquivo de vagas de trabalho', 'PostType to make work stations' ),
		'attributes'            => __( 'Item Attributes', 'PostType to make work stations' ),
		'parent_item_colon'     => __( 'Carreira:', 'PostType to make work stations' ),
		'all_items'             => __( 'Todas as vagas', 'PostType to make work stations' ),
		'add_new_item'          => __( 'Adicionar nova vaga', 'PostType to make work stations' ),
		'add_new'               => __( 'Adicionar novo', 'PostType to make work stations' ),
		'new_item'              => __( 'Nova vaga', 'PostType to make work stations' ),
		'edit_item'             => __( 'Editar vaga', 'PostType to make work stations' ),
		'update_item'           => __( 'Atualizar vaga', 'PostType to make work stations' ),
		'view_item'             => __( 'Ver vaga', 'PostType to make work stations' ),
		'view_items'            => __( 'Ver vagas', 'PostType to make work stations' ),
		'search_items'          => __( 'Procurar por vaga', 'PostType to make work stations' ),
		'not_found'             => __( 'Não encontrado', 'PostType to make work stations' ),
		'not_found_in_trash'    => __( 'Não encontrado no lixo', 'PostType to make work stations' ),
		'featured_image'        => __( 'Imagem de destaque', 'PostType to make work stations' ),
		'set_featured_image'    => __( 'Escolher como imagem de destaque', 'PostType to make work stations' ),
		'remove_featured_image' => __( 'Remover imagem de destaque', 'PostType to make work stations' ),
		'use_featured_image'    => __( 'Usar como imagem de destaque', 'PostType to make work stations' ),
		'insert_into_item'      => __( 'Inserir dentro', 'PostType to make work stations' ),
		'uploaded_to_this_item' => __( 'Atualizado para essa vaga', 'PostType to make work stations' ),
		'items_list'            => __( 'Lista de vagas', 'PostType to make work stations' ),
		'items_list_navigation' => __( 'Lista navegavel de vagas', 'PostType to make work stations' ),
		'filter_items_list'     => __( 'Filtrar lista de vagas', 'PostType to make work stations' ),
	);
	$args = array(
		'label'                 => __( 'Vaga de Trabalho', 'PostType to make work stations' ),
		'description'           => __( 'Vagas de trabalho no WillGo', 'PostType to make work stations' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'taxonomies'            => array( 'careers', 'place' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_icon'             => 'dashicons-businessman',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'work_at_willgo', $args );

}
add_action( 'init', 'work_at_willgo', 0 );

// Register Custom Taxonomy
function careers() {

	$labels = array(
		'name'                       => _x( 'Carreiras', 'Taxonomy General Name', 'Carreiras para o WillGo Brasil' ),
		'singular_name'              => _x( 'Carreira', 'Taxonomy Singular Name', 'Carreiras para o WillGo Brasil' ),
		'menu_name'                  => __( 'Carreiras', 'Carreiras para o WillGo Brasil' ),
		'all_items'                  => __( 'Todas as carreiras', 'Carreiras para o WillGo Brasil' ),
		'parent_item'                => __( 'Carreira pai', 'Carreiras para o WillGo Brasil' ),
		'parent_item_colon'          => __( 'Carreira pai:', 'Carreiras para o WillGo Brasil' ),
		'new_item_name'              => __( 'Nova carreira', 'Carreiras para o WillGo Brasil' ),
		'add_new_item'               => __( 'Acidionar nova carreira', 'Carreiras para o WillGo Brasil' ),
		'edit_item'                  => __( 'Editar carreira', 'Carreiras para o WillGo Brasil' ),
		'update_item'                => __( 'Atualizar carreira', 'Carreiras para o WillGo Brasil' ),
		'view_item'                  => __( 'Ver carreira', 'Carreiras para o WillGo Brasil' ),
		'separate_items_with_commas' => __( 'Separar carreiras por virgulas', 'Carreiras para o WillGo Brasil' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover carreiras', 'Carreiras para o WillGo Brasil' ),
		'choose_from_most_used'      => __( 'Escolher dentre os mais utilizados', 'Carreiras para o WillGo Brasil' ),
		'popular_items'              => __( 'Carreiras populares', 'Carreiras para o WillGo Brasil' ),
		'search_items'               => __( 'Procurar por carreiras', 'Carreiras para o WillGo Brasil' ),
		'not_found'                  => __( 'Não encontrado', 'Carreiras para o WillGo Brasil' ),
		'no_terms'                   => __( 'Nenhuma carreira', 'Carreiras para o WillGo Brasil' ),
		'items_list'                 => __( 'Lista de carreiras', 'Carreiras para o WillGo Brasil' ),
		'items_list_navigation'      => __( 'Lista navegavel de carreiras', 'Carreiras para o WillGo Brasil' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'careers', array( 'work_at_willgo' ), $args );

}
add_action( 'init', 'careers', 0 );

// Register Custom Taxonomy
function place() {

	$labels = array(
		'name'                       => _x( 'Local de tabalho', 'Taxonomy General Name', 'Locais de trabalho WillGoBrasil' ),
		'singular_name'              => _x( 'Local de trabalho', 'Taxonomy Singular Name', 'Locais de trabalho WillGoBrasil' ),
		'menu_name'                  => __( 'Locais', 'Locais de trabalho WillGoBrasil' ),
		'all_items'                  => __( 'Todos os locais', 'Locais de trabalho WillGoBrasil' ),
		'parent_item'                => __( 'Local pai', 'Locais de trabalho WillGoBrasil' ),
		'parent_item_colon'          => __( 'Local pai:', 'Locais de trabalho WillGoBrasil' ),
		'new_item_name'              => __( 'Novo local', 'Locais de trabalho WillGoBrasil' ),
		'add_new_item'               => __( 'Acidionar novo local', 'Locais de trabalho WillGoBrasil' ),
		'edit_item'                  => __( 'Editar local', 'Locais de trabalho WillGoBrasil' ),
		'update_item'                => __( 'Atualizar local', 'Locais de trabalho WillGoBrasil' ),
		'view_item'                  => __( 'Ver local', 'Locais de trabalho WillGoBrasil' ),
		'separate_items_with_commas' => __( 'Separar locais por virgulas', 'Locais de trabalho WillGoBrasil' ),
		'add_or_remove_items'        => __( 'Adicionar ou remover local', 'Locais de trabalho WillGoBrasil' ),
		'choose_from_most_used'      => __( 'Escolher dentre os mais utilizados', 'Locais de trabalho WillGoBrasil' ),
		'popular_items'              => __( 'Locais mais utilizados', 'Locais de trabalho WillGoBrasil' ),
		'search_items'               => __( 'Procurar por locais', 'Locais de trabalho WillGoBrasil' ),
		'not_found'                  => __( 'Não encontrado', 'Locais de trabalho WillGoBrasil' ),
		'no_terms'                   => __( 'Nenhum local', 'Locais de trabalho WillGoBrasil' ),
		'items_list'                 => __( 'Lista de locais', 'Locais de trabalho WillGoBrasil' ),
		'items_list_navigation'      => __( 'Lista navegavel de locais', 'Locais de trabalho WillGoBrasil' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'place', array( 'work_at_willgo' ), $args );

}
add_action( 'init', 'place', 0 );


?>
