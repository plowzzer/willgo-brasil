<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package onegate_theme
 */

get_header(); ?>

<div class="workatwillgo">
	<div style="padding-top:90px;"></div>
	<div class="uk-container uk-container-center">
		<header class="page-header">
			<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="taxonomy-description">', '</div>' );
			?>
		</header><!-- .page-header -->

		<?php
		if ( have_posts() ) : ?>
			<h2>Todas as vagas</h2>
			<table class="uk-table uk-table-striped">
				<caption>Todas as vagas disponiveis para esse local</caption>
				<thead>
						<tr>
								<th>Nome da Vaga</th>
								<th>Carreira</th>
								<th>Local</th>
						</tr>
				</thead>

				<tbody>


				<?php
				while ( have_posts() ) : the_post();?>

					<tr>
						<td><a href="<?the_permalink();?>"><?the_title();?></a> <small>ID <?= $post->ID;?></small></td>
						<td>
							<?$term_list_careers = wp_get_post_terms($post->ID, 'careers', array("parent"=>0));?>
							<?foreach($term_list_careers as $term_single) {?>
								<a href="<?= esc_url( home_url( '/' ) ); ?>careers/<?=$term_single->slug;?>"><?=$term_single->name;?></a>
							<?}?>
						</td>

						<td>
							<?$term_list_place = wp_get_post_terms($post->ID, 'place', array("parent"=>0));?>
							<?foreach($term_list_place as $term_single) {?>
								<a href="<?= esc_url( home_url( '/' ) ); ?>place/<?=$term_single->slug;?>"><?=$term_single->name;?></a>
							<?}?>
						</td>
					</tr>

				<?endwhile;?>

				</tbody>
			</table>


		<?	the_posts_navigation();

		else :

			echo "<p style='text-align:center;'>Não há nenhuma vaga no momento, volte mais tarde</p>";

		endif; ?>

	</div><!--container-->
</div><!--page-->

<?get_footer();
