<?php
/**
*
*Template Name: Planos
*Template texto: Usar como pagina WillGo Planos
*
* @package one
*/

get_header(); ?>

<div class="plans">
  <?$background = get_field('banner');?>
  <div  class="banner" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
  </div>

  <h1 class="title">Planos</h1>

  <section class="adv" id="adv">
    <div class="uk-container uk-container-center">
      <h1>Vantagens do Aplicativo</h1>
      <ul class="uk-grid">
        <?php if( have_rows('vantagem') ):
          while ( have_rows('vantagem') ) : the_row();
            $image = get_sub_field('imagem'); ?>
            <li class="uk-width-1-1 uk-width-medium-1-2" style="margin-bottom: 15px">
              <div class="uk-grid">
                <div class="uk-width-1-4">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </div>
                <div class="uk-width-3-4">
                	<h4><?php the_sub_field('titulo');?> </h4>
                  <?php the_sub_field('texto');?>
                </div>
              </div>
            </li>
          <?endwhile;
        endif; ?>
      </ul>
      <div class="uk-width-1-4 uk-hidden-small"> </div>
    </div>
  </section>

  <section class="videos" id="videos">
    <h1>Assista os nossos vídeos</h1>
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <? if( have_rows('videos') ):
          while ( have_rows('videos') ) : the_row();?>
            <div class="uk-width-1-1 uk-width-medium-1-3" style="margin-bottom: 15px">
              <a href="<?the_sub_field('video-link');?>" data-uk-lightbox>
                <?$image=get_sub_field('video-image');?>
                <img src="<?= $image['url']; ?>" alt="" />
              </a>
            </div>
          <?endwhile;
        endif; ?>
        <div class="uk-width-1-3">
          <a href="<? the_field('video-link'); ?>">
            <img src="<?the_field('video-img');?>" alt="" />
          </a>

        </div>
      </div>
    </div>
  </section>

  <div class="perspectiva">
    <div class="uk-container uk-container-center" style="text-align:center;">
      <h1>Perspectiva de ganhos</h1>
      <?$image_p = get_field('perspectiva-image');?>
      <img src="<?= $image_p['url']; ?>" alt="" />
      <p style="text-align:right;">
        <small>*60km por dia, durante 30 dias</small>
      </p>
    </div>
  </div>

  <p style="text-align:center;">
    <a class="cadastro-btn" href="https://www.willgobrasil.com.br/novo/site-pack-motorista-cadastro">Cadastre-se</a>
  </p>


<?php get_footer(); ?>
