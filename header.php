<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package onegate_theme
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0 !important;">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/fonts/fonts.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/uikit.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/slideshow.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/slidenav.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/slider.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/components/dotnav.min.css" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<link href='https://fonts.googleapis.com/css?family=Archivo+Black' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500,300italic,300,400italic' rel='stylesheet' type='text/css'>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/uikit.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/smooth-scroll.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/dropdown.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/scrollspy.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/core/modal.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slider.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/accordion.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/parallax.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slideshow.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slideshow-fx.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/lightbox.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/slideset.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/components/grid.min.js"></script>

	<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script> -->

  <!-- <script type="text/javascript">
		$(window).load(function() {
			$(".overlay").fadeOut(1500);
		});
	</script> -->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php if (is_page( 'empreendedor' )): ?>
		<header id="entrepreneur">
			<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
				<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
			</div>

			<div id="new-menu" class="uk-offcanvas">
		    <div class="uk-offcanvas-bar">
		      <ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
						<li class="uk-nav-header">Navegação</li>
						<li><a href="#one" data-uk-smooth-scroll="{offset: 91}">Oportunidade de Negócios</a></li>
						<li><a href="#video" data-uk-smooth-scroll="{offset: 91}">Como Funciona</a></li>
						<li><a href="#adv" data-uk-smooth-scroll="{offset: 91}">Vantagens do Aplicativo</a></li>
						<li><a href="#features" data-uk-smooth-scroll="{offset: 91}">Recursos do Aplicativo</a></li>
						<li><a href="#planos" data-uk-smooth-scroll="{offset: 91}">Mobilidade</a></li>
						<li><a href="#ja-call" data-uk-smooth-scroll="{offset: 91}">WillGo Já</a></li>
						<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
						<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
						<li class="uk-nav-header uk-hidden-medium uk-hidden-large">Painel</li>
						<li><a class="uk-hidden-medium uk-hidden-large" href="http://willgobrasil.com.br/novo/passageiro"><i class="fa fa-user"></i> Usuário</a></li>
						<li><a class="uk-hidden-medium uk-hidden-large" href="http://willgobrasil.com.br/novo/empreendedor"><i class="fa fa-user-plus"></i> Empreendedor</a></li>
						<li class="uk-nav-header">Páginas</li>
						<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
						<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
						<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
						<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
						<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
						<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
						<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
						<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
						<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
						<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
						<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
						<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
						<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
						<li><a href="http://www.willgobrasil.com.br/seego"><i class="fa fa-arrow-right" aria-hidden="true"></i> Seego</a></li>
						<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
					</ul>
		    </div>
			</div>

			<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
			<ul id="session" class="uk-hidden-small">
				<li><a href="http://willgobrasil.com.br/novo/empreendedor"><i class="fa fa-user-plus"></i><br>Empreendedor</a></li>
			</ul>
			<span id="admin-call" class="uk-hidden-small">Acesse o seu painel <i class="fa fa-chevron-right"></i></span>
		</header>
		<span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span>
	<?php elseif (is_page( 'cliente' )): ?>
			<header id="user">
				<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
					<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
				</div>

				<div id="new-menu" class="uk-offcanvas">
			    <div class="uk-offcanvas-bar">
			      <ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
							<li class="uk-nav-header">Navegação</li>
							<li><a href="#one" data-uk-smooth-scroll="{offset: 91}">Quanto vai Sair</a></li>
							<li><a href="#adv" data-uk-smooth-scroll="{offset: 91}">Favorite seu Motorista</a></li>
							<li><a href="#objects" data-uk-smooth-scroll="{offset: 91}">Passageiros e Objetos</a></li>
							<li><a href="#cancel" data-uk-smooth-scroll="{offset: 91}">Agendamento 48 Horas</a></li>
							<li><a href="#varieties" data-uk-smooth-scroll="{offset: 91}">Variedade de Carros</a></li>
							<li><a href="#promo" data-uk-smooth-scroll="{offset: 91}"><i class="fa fa-star"></i> PREMIUM</a></li>
							<li><a href="#ja-call" data-uk-smooth-scroll="{offset: 91}">WillGo Já</a></li>

							<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
							<!-- <li><a href="">Sobre o WillGO</a></li>
							<li><a href="">Baixar Aplicativo</a></li> -->
							<li class="uk-nav-header uk-hidden-medium uk-hidden-large">Painel</li>
							<li><a class="uk-hidden-medium uk-hidden-large" href="http://willgobrasil.com.br/novo/passageiro"><i class="fa fa-user"></i> Usuário</a></li>
							<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
							<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
							<li class="uk-nav-header">Páginas</li>
							<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
							<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
							<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
							<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
							<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
							<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
							<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
							<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
							<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
							<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
							<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
							<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
							<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
							<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
						</ul>
			    </div>
				</div>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
				<p class="uk-hidden-small"><small>Download and GO!</small></p>
				<ul id="session" class="uk-hidden-small">
					<li><a href="http://willgobrasil.com.br/novo/passageiro"><span><i class="fa fa-user"></i><br>Cliente</a></span></li>
				</ul>
				<span id="admin-call" class="uk-hidden-small">Acesse o seu painel <i class="fa fa-chevron-right"></i></span>
			</header>
			<span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span>
		<?php elseif (is_page( 'ja' )): ?>
				<header id="ja">

					<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
						<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
					</div>

					<div id="new-menu" class="uk-offcanvas">
				    <div class="uk-offcanvas-bar">
				      <ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
								<li class="uk-nav-header">Navegação</li>
								<li><a href="#about" data-uk-smooth-scroll="{offset: 91}">Sobre</a></li>
								<li><a href="#sameday" data-uk-smooth-scroll="{offset: 91}">Same Day</a></li>
								<li><a href="#paravoce" data-uk-smooth-scroll="{offset: 91}">Para Você</a></li>
								<li><a href="#paraempresa" data-uk-smooth-scroll="{offset: 91}">Para sua Empresa</a></li>
								<li><a href="#suascompras" data-uk-smooth-scroll="{offset: 91}">Compras</a></li>
								<li><a href="#ja-call" data-uk-smooth-scroll="{offset: 91}">Delivery</a></li>
								<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
								<!-- <li><a href="">Sobre o WillGO</a></li>
								<li><a href="">Baixar Aplicativo</a></li> -->
								<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
								<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
								<li class="uk-nav-header">Páginas</li>
								<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
								<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
								<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
								<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
								<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
								<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
								<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
								<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
								<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
								<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
								<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
								<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
								<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
								<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
							</ul>
				    </div>
					</div>

					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_ja.svg" alt="Logo WillGo Já" /></a>
					<p class="uk-hidden-small"><small>Download and GO!</small></p>
					<ul id="session" class="uk-hidden-small">
						<li><a href="http://willgobrasil.com.br/novo/cadastro"><span><i class="fa fa-user"></i><br>Cadastre-se</a></span></li>
					</ul>
				</header>
				<span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span>


			<?php elseif (is_page( 'escolar' )): ?>
					<header id="ja">

						<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
							<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
						</div>

						<div id="new-menu" class="uk-offcanvas">
							<div class="uk-offcanvas-bar">
								<ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
									<li class="uk-nav-header">Navegação</li>
									<li><a href="#team2" data-uk-smooth-scroll="{offset: 91}">Time</a></li>
									<li><a href="#solucao" data-uk-smooth-scroll="{offset: 91}">Soluções para vocês</a></li>
									<li><a href="#sobre" data-uk-smooth-scroll="{offset: 91}">Sobre</a></li>
									<li><a href="#plano" data-uk-smooth-scroll="{offset: 91}">Plano</a></li>
									<li><a href="#training" data-uk-smooth-scroll="{offset: 91}">Treinamento</a></li>
									<li><a href="#cad" data-uk-smooth-scroll="{offset: 91}">Cadastrar</a></li>
									<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
									<!-- <li><a href="">Sobre o WillGO</a></li>
									<li><a href="">Baixar Aplicativo</a></li> -->
									<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
									<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
									<li class="uk-nav-header">Páginas</li>
									<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
									<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
									<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
									<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
									<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
									<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
									<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
									<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
									<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
									<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
									<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
									<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
									<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
									<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
								</ul>
							</div>
						</div>

						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo Já" /></a>
						<p class="uk-hidden-small"><small>Download and GO!</small></p>
						<!-- <ul id="session" class="uk-hidden-small">
							<li><a href="http://willgobrasil.com.br/novo/cadastro"><span><i class="fa fa-user"></i><br>Cadastre-se</a></span></li>
						</ul> -->
					</header>
					<!-- <span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span> -->



		<?php elseif (is_page( 'franchising' )): ?>
				<header id="ja">

					<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
						<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
					</div>

					<div id="new-menu" class="uk-offcanvas">
				    <div class="uk-offcanvas-bar">
				      <ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
								<li class="uk-nav-header">Navegação</li>
								<li><a href="#about" data-uk-smooth-scroll="{offset: 91}">Sobre</a></li>
								<li><a href="#merc" data-uk-smooth-scroll="{offset: 91}">O Mercado</a></li>
								<li><a href="#concpt" data-uk-smooth-scroll="{offset: 91}">O Conceito</a></li>
								<li><a href="#sos" data-uk-smooth-scroll="{offset: 91}">Suporte ao franqueado</a></li>
								<li><a href="#atuacao" data-uk-smooth-scroll="{offset: 91}">Diferenciais</a></li>
								<li><a href="#raio" data-uk-smooth-scroll="{offset: 91}">Raio de atuação</a></li>
								<li><a href="#contact" data-uk-smooth-scroll="{offset: 91}">Cadastre-se</a></li>
								<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
								<!-- <li><a href="">Sobre o WillGO</a></li>
								<li><a href="">Baixar Aplicativo</a></li> -->
								<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
								<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
								<li class="uk-nav-header">Páginas</li>
								<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
								<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
								<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
								<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
								<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
								<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
								<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
								<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
								<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
								<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
								<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
								<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
								<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
								<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
							</ul>
				    </div>
					</div>

					<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_franchising.svg" alt="Logo WillGo" /></a>
				</header>
				<!-- <span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span> -->

			<?php elseif (is_page( 'embaixador' )): ?>
					<header id="ja">

						<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
							<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
						</div>

						<div id="new-menu" class="uk-offcanvas">
							<div class="uk-offcanvas-bar">
								<ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
									<li class="uk-nav-header">Navegação</li>
									<li><a href="#faturar" data-uk-smooth-scroll="{offset: 91}">Como Faturar</a></li>
									<li><a href="#bonus" data-uk-smooth-scroll="{offset: 91}">Bônus</a></li>
									<li><a href="#exp" data-uk-smooth-scroll="{offset: 91}">Experiencia</a></li>
									<li><a href="#depoimentos" data-uk-smooth-scroll="{offset: 91}">Depoimentos</a></li>
									<li><a href="#respostas" data-uk-smooth-scroll="{offset: 91}">Perguntas e Respostas</a></li>
									<li><a href="#contact" data-uk-smooth-scroll="{offset: 91}">Seja Embaixador</a></li>
									<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
									<!-- <li><a href="">Sobre o WillGO</a></li>
									<li><a href="">Baixar Aplicativo</a></li> -->
									<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
									<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
									<li class="uk-nav-header">Páginas</li>
									<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
									<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
									<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
									<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
									<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
									<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
									<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
									<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
									<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
									<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
									<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
									<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
									<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
									<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
								</ul>
							</div>
						</div>

						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
					</header>
					<!-- <span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span> -->

			<?php elseif (is_page( 'cargo' )): ?>
					<header id="ja">

						<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
							<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
						</div>

						<div id="new-menu" class="uk-offcanvas">
					    <div class="uk-offcanvas-bar">
					      <ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
									<li class="uk-nav-header">Navegação</li>
									<li><a href="#beneficios" data-uk-smooth-scroll="{offset: 91}">Beneficios</a></li>
									<li><a href="#para_voce" data-uk-smooth-scroll="{offset: 91}">Para Você</a></li>
									<li><a href="#sua_empresa" data-uk-smooth-scroll="{offset: 91}">Para sua Empresa</a></li>
									<li><a href="#delivery" data-uk-smooth-scroll="{offset: 91}">Para suas Entregas</a></li>
									<li><a href="#planos" data-uk-smooth-scroll="{offset: 91}">Tarifas</a></li>
									<li><a href="http://willgobrasil.com.br/novo/cadastro">Cadastre-se</a></li>

									<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
									<!-- <li><a href="">Sobre o WillGO</a></li>
									<li><a href="">Baixar Aplicativo</a></li> -->
									<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
									<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
									<li class="uk-nav-header">Páginas</li>
									<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
									<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
									<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
									<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
									<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
									<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
									<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
									<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
									<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
									<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
									<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
									<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
									<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
									<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
								</ul>
					    </div>
						</div>

						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo-cargo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_cargo.svg" alt="Logo WillGo" /></a>
						<ul id="session" class="uk-hidden-small">
							<li><a href="http://willgobrasil.com.br/novo/cadastro"><span><i class="fa fa-user"></i><br>Cadastre-se</a></span></li>
						</ul>
					</header>
					<span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span>


				<?php elseif (is_page( 'corporate' )): ?>
						<header id="ja">

							<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
								<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
							</div>

							<div id="new-menu" class="uk-offcanvas">
						    <div class="uk-offcanvas-bar">
						      <ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
										<li class="uk-nav-header">Navegação</li>
										<li><a href="#como-funciona" data-uk-smooth-scroll="{offset: 91}">Como Funciona</a></li>
										<li><a href="#despesas" data-uk-smooth-scroll="{offset: 91}">Despesas</a></li>
										<li><a href="#seguranca" data-uk-smooth-scroll="{offset: 91}">Segurança</a></li>
										<li><a href="#contact" data-uk-smooth-scroll="{offset: 91}">Cadastre-se</a></li>
										<!-- <li class="uk-hidden-medium uk-hidden-large uk-nav-header">Outros</li> -->
										<!-- <li><a href="">Sobre o WillGO</a></li>
										<li><a href="">Baixar Aplicativo</a></li> -->
										<li class="uk-hidden-medium uk-hidden-large uk-nav-header">Suporte</li>
										<li class="uk-hidden-medium uk-hidden-large"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><i class="fa fa-comment"></i> Suporte</a></li>
										<li class="uk-nav-header">Páginas</li>
										<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
										<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
										<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
										<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
										<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
										<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
										<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
										<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
										<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
										<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
										<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
										<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
										<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
										<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
									</ul>
						    </div>
							</div>

							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_corporate.svg" alt="Logo WillGo" /></a>
							<ul id="session" class="uk-hidden-small">
								<li><a href="http://willgobrasil.com.br/novo/cadastro"><span><i class="fa fa-user"></i><br>Cadastre-se</a></span></li>
							</ul>
						</header>
						<!-- <span id="suport" class="uk-hidden-small"><a target="_blank" href="http://ajuda.willgobrasil.com.br/"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/suport_chat.png" alt="Suporte via Chat" /></a></span> -->



		<?php elseif (is_front_page()): ?>
		<?php else : ?>
			<header id="entrepreneur">

				<div class="uk-button-dropdown" data-uk-offcanvas="{target:'#new-menu'}">
					<button class="nav-button uk-button"><i class="fa fa-bars"></i><br><span>Menu</span></button>
				</div>

				<div id="new-menu" class="uk-offcanvas">
					<div class="uk-offcanvas-bar">
						<ul class="uk-nav uk-nav-offcanvas" data-uk-nav>
							<li class="uk-nav-header">Páginas</li>
							<li><a href="http://www.willgobrasil.com.br/empreendedor"><i class="fa fa-user-plus"></i> Sou Empreendedor</a></li>
							<li><a href="http://www.willgobrasil.com.br/cliente"><i class="fa fa-user"></i> Sou Cliente</a></li>
							<li><a href="http://www.willgobrasil.com.br/ja"><i class="fa fa-motorcycle" aria-hidden="true"></i> WillGo Já</a></li>
							<li><a href="http://www.willgobrasil.com.br/corporate"><i class="fa fa-tachometer" aria-hidden="true"></i> Corporate</a></li>
							<li><a href="http://www.willgobrasil.com.br/franchising"><i class="fa fa-users" aria-hidden="true"></i> Franquia</a></li>
							<li><a href="http://www.willgobrasil.com.br/cargo"><i class="fa fa-truck" aria-hidden="true"></i> Cargo</a></li>
							<li><a href="http://www.willgobrasil.com.br/evento-perfeito"><i class="fa fa-map-marker" aria-hidden="true"></i> Evento Perfeito</a></li>
							<li><a href="http://www.willgobrasil.com.br/assessoria-de-imprensa"><i class="fa fa-camera" aria-hidden="true"></i> Assessoria de Imprensa</a></li>
							<li><a href="http://www.willgobrasil.com.br/apoios-e-parceiros"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Apoio e Parcerias</a></li>
							<li><a href="http://www.willgobrasil.com.br/locais"><i class="fa fa-globe" aria-hidden="true"></i> Cidades Atendidas</a></li>
							<li><a href="http://www.willgobrasil.com.br/embaixador"><i class="fa fa-star" aria-hidden="true"></i> Seja um Embaixador</a></li>
							<li><a href="http://www.willgobrasil.com.br/work_at_willgo"><i class="fa fa-briefcase" aria-hidden="true"></i> Trabalhe no WillGo</a></li>
							<li><a href="http://www.queroserwillgo.com.br"><i class="fa fa-car"></i> Quero ser WillGo</a></li>
							<li><a href="http://www.willgobrasil.com.br/downloads"><i class="fa fa-download"></i> Downloads</a></li>
						</ul>
					</div>
				</div>

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
			</header>
	<?php endif; ?>
