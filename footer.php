<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package onegate_theme
 */

?>

	</div><!-- #content -->
	<?php if (is_page( 'empreendedor' )): ?>
		<footer role="contentinfo">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="brand uk-width-medium-1-4 uk-width-small-1-1">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
						<p><small>WillGo, Download and GO!</small></p>
						<small>© Copyright 2016</small>
					</div>
					<ul class="menu uk-width-medium-1-4 uk-width-small-1-1">
						<li><a href="#one" data-uk-smooth-scroll="{offset: 91}">Oportunidade de Negócios</a></li>
						<li><a href="#video" data-uk-smooth-scroll="{offset: 91}">Como Funciona</a></li>
						<li><a href="#adv" data-uk-smooth-scroll="{offset: 91}">Vantagens do Aplicativo</a></li>
						<li><a href="#features" data-uk-smooth-scroll="{offset: 91}">Recursos do Aplicativo</a></li>
						<li><a href="#plans" data-uk-smooth-scroll="{offset: 91}">Mobilidade</a></li>
						<li><a href="#ja-call" data-uk-smooth-scroll="{offset: 91}">Delivery</a></li>
						<!-- <li><a href="#">Seja Premium</a></li> -->
						<!-- <li><a href="#">Perguntas Frequentes</a></li> -->
					</ul>
					<div class="app uk-width-medium-1-4 uk-width-small-1-1">
						<small>Baixe o aplicativo</small><br>
						<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/applestore.png" alt="Apple Store" /></a>
						<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/googleplay.png" alt="Google Play" /></a>
					</div>
					<div class="item uk-width-medium-1-4 uk-width-small-1-1">
						<div class="fb-page" data-href="https://www.facebook.com/WillGoBrasil/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/WillGoBrasil/"><a href="https://www.facebook.com/WillGoBrasil/">WillGo Brasil</a></blockquote></div></div>
					</div>
				</div>
			</div>
			<!-- Facebook Pixel Code for Emtrepreneur -->
			<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','//connect.facebook.net/en_US/fbevents.js');

			fbq('init', '980014992034515');
			fbq('track', "PageView");</script>
			<noscript><img height="1" width="1" style="display:none"
			src="https://www.facebook.com/tr?id=980014992034515&ev=PageView&noscript=1"
			/></noscript>
			<!-- End Facebook Pixel Code -->
		</footer><!-- #colophon -->
	<?php elseif (is_page( 'cliente' )): ?>
		<footer role="contentinfo">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="brand uk-width-medium-1-4 uk-width-small-1-1">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
						<p><small>WillGo, Download and GO!</small></p>
						<small>© Copyright 2016</small>
					</div>
					<div class="app uk-width-medium-2-4 uk-width-small-1-1">
						<small>Baixe o aplicativo</small><br>
						<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/applestore.png" alt="Apple Store" /></a>
						<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/googleplay.png" alt="Google Play" /></a>					</div>
					<div class="item uk-width-medium-1-4 uk-width-small-1-1">
						<div class="fb-page" data-href="https://www.facebook.com/WillGoBrasilPremium/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/WillGoBrasilPremium/"><a href="https://www.facebook.com/WillGoBrasilPremium/">WillGo Brasil Premium</a></blockquote></div></div>
					</div>
				</div>
			</div>

		</footer><!-- #colophon -->
		<!-- Facebook Pixel Code - For user -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','//connect.facebook.net/en_US/fbevents.js');

		fbq('init', '577843109057738');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=577843109057738&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->
	<?php elseif (is_page( 'ja' )): ?>
		<footer role="contentinfo">
			<div class="uk-container uk-container-center">
				<div class="uk-grid">
					<div class="brand uk-width-medium-1-4 uk-width-small-1-1">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
						<p><small>WillGo, Download and GO!</small></p>
						<small>© Copyright 2016</small>
					</div>
					<ul class="menu uk-width-medium-1-4 uk-width-small-1-1">
						<li><a href="#about" data-uk-smooth-scroll="{offset: 91}">Sobre</a></li>
						<li><a href="#paravoce" data-uk-smooth-scroll="{offset: 91}">Para Você</a></li>
						<li><a href="#paraempresa" data-uk-smooth-scroll="{offset: 91}">Para sua Empresa</a></li>
						<li><a href="#suascompras" data-uk-smooth-scroll="{offset: 91}">Compras</a></li>
						<li><a href="#ja-call" data-uk-smooth-scroll="{offset: 91}">Delivery</a></li>
						<!-- <li><a href="#">Seja Premium</a></li> -->
						<!-- <li><a href="#">Perguntas Frequentes</a></li> -->
					</ul>
					<div class="app uk-width-medium-1-4 uk-width-small-1-1">
						<small>Baixe o aplicativo</small><br>
						<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/applestore.png" alt="Apple Store" /></a>
						<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/googleplay.png" alt="Google Play" /></a>
					</div>
					<div class="item uk-width-medium-1-4 uk-width-small-1-1">
						<div class="fb-page" data-href="https://www.facebook.com/WillGoBrasil/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/WillGoBrasil/"><a href="https://www.facebook.com/WillGoBrasil/">WillGo Brasil</a></blockquote></div></div>
					</div>
				</div>
			</div>

		</footer><!-- #colophon -->

	<?php elseif (is_page( 'evento-perfeito' ) || is_page( 'assessoria-de-imprensa' ) || is_page ('parceiros') || is_page ('locais') || is_page ('downloads') || is_page ('cargo') || is_page ('franchising') || is_page ('corporate') || is_page ('ribeirao-preto') || is_page ('embaixador') || is_page ('planos') || is_page ('escolar') || is_post_type_archive ('work_at_willgo') || is_singular ('work_at_willgo') ): ?>
			<footer role="contentinfo">
				<div class="uk-container uk-container-center">
					<div class="uk-grid">
						<div class="brand uk-width-medium-1-3 uk-width-small-1-1">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/willgo_white.svg" alt="Logo WillGo" /></a>
							<p><small>WillGo, Download and GO!</small></p>
							<small>© Copyright 2016</small>
						</div>
						<div class="app uk-width-medium-1-3 uk-width-small-1-1">
							<small>Baixe o aplicativo</small><br>
							<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/applestore.png" alt="Apple Store" /></a>
							<a href="http://www.willgobrasil.com.br/downloads"><img src="<?=bloginfo('stylesheet_directory')?>/assets/img/googleplay.png" alt="Google Play" /></a>
						</div>
						<div class="item uk-width-medium-1-3 uk-width-small-1-1">
							<div class="fb-page" data-href="https://www.facebook.com/WillGoBrasil/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/WillGoBrasil/"><a href="https://www.facebook.com/WillGoBrasil/">WillGo Brasil</a></blockquote></div></div>
						</div>
					</div>
				</div>

			</footer><!-- #colophon -->
			<?if (is_page( 'downloads' )) {?>

				<!-- Facebook Pixel Code - For DOWNLOADS -->
				<script>
				!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
				n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
				document,'script','https://connect.facebook.net/en_US/fbevents.js');

				fbq('init', '980014992034515');
				fbq('track', "PageView");</script>
				<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=980014992034515&ev=PageView&noscript=1"
				/></noscript>

				<!-- End Facebook Pixel Code -->
			<?}?>

		<?php endif; ?>

</div><!-- #page -->
<?php get_template_part( 'template-parts/facebook' ); ?>
<?php get_template_part( 'template-parts/google' ); ?>

<?php wp_footer(); ?>

</body>
</html>
