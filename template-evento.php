<?php
/**
*
*Template Name: Evento Perfeito
*Template texto: Usar como pagina WillGo Evento Perfeito
*
* @package one
*/

get_header(); ?>

<div class="evento">
  <div class="uk-container uk-container-center into">
    <?php while ( have_posts() ) : the_post();
      the_content();
    endwhile; ?>
    <?php $images = get_field('tipos_de_evento');

    if( $images ): ?>
      <ul class="uk-grid uk-grid-collapse">
        <?php foreach( $images as $image ): ?>
          <li class="uk-width-1-2 uk-width-medium-1-4">
             <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
          </li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  </div>
  <div class="formulario">
    <div class="uk-container uk-container-center">
        <?php $evento_form = get_field('evento_form');
        echo do_shortcode($evento_form);?>
      </div>
      <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div>

</div>


<?php get_footer(); ?>
