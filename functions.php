<?php
/**
 * onegate_theme functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package onegate_theme
 */

require_once 'lib/plugins/pl-plugins.php';
require_once 'lib/post_types/parceiros.php';
require_once 'lib/post_types/work_stations.php';

require_once 'wp-advanced-search/wpas.php';


if ( ! function_exists( 'one_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function one_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on onegate_theme, use a find and replace
	 * to change 'one' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'one', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'one' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'one_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'one_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function one_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'one_content_width', 640 );
}
add_action( 'after_setup_theme', 'one_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function one_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'one' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'one_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function one_scripts() {
	wp_enqueue_style( 'one-style', get_stylesheet_uri() );

	wp_enqueue_script( 'one-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'one-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'one_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

if( function_exists('acf_add_options_page') ) {

	$option_page = acf_add_options_page(array(
		'page_title' 	=> 'Trabalhe na WillGo',
		'menu_title' 	=> 'Work at WillGo',
		'menu_slug' 	=> 'workat-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));

}

function my_acf_google_map_api( $api ){

	$api['key'] = 'AIzaSyDbhmtNJPmNSMfy4CsO4LtnSm2NmatMZrw';

	return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function work_search() {
    $args = array();
    $args['wp_query'] = array('post_type' => 'work_at_willgo',
                              'posts_per_page' => 30,
															'orderby' => 'title',
	                            'order' => 'ASC');
		//$args['form'] = array( 'auto_submit' => true );

		$args['form']['ajax'] = array( 'enabled' => true,
															'show_default_results' => true,
															'results_template' => 'search-advanced.php', // This file must exist in your theme root
															'button_text' => 'Ver mais resultados');

    $args['fields'][] = array('type' => 'search',
                              'title' => 'Search',
                              'placeholder' => 'Palavra-chave');
    $args['fields'][] = array('type' => 'taxonomy',
                              'taxonomy' => 'careers',
                              'format' => 'select',
															'allow_null' => "Selecione uma carreira");
		$args['fields'][] = array('type' => 'taxonomy',
                              'taxonomy' => 'place',
                              'format' => 'select',
															'allow_null' => "Selecione um local");
		$args['fields'][] = array( 'type' => 'submit',
													     'class' => 'button',
													     'value' => 'Buscar' );
    register_wpas_form('work_search', $args);
}
add_action('init', 'work_search');
