<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package onegate_theme
 */

get_header(); ?>

<div class="workatwillgo">


	<div class="uk-container uk-container-center">
		<div class="page-header">
		<? while ( have_posts() ) : the_post();

			$careers = wp_get_post_terms($post->ID, 'careers', array("parent"=>0));?>
			<div class="career_call">
				Carreira:
				<?foreach($careers as $careers_single) {?>
					<a href="<?= esc_url( home_url( '/' ) ); ?>careers/<?= $careers_single->slug;?>"><?= $careers_single->name;?></a>
					<?= " "; ?>
				<?}?>
			</div>

			<?the_title('<h1>','</h1>');
			$place = wp_get_post_terms($post->ID, 'place', array("parent"=>0));?>
			<div class="place_call">
				Local:
				<?foreach($place as $place_single) {?>
					<a href="<?= esc_url( home_url( '/' ) ); ?>place/<?= $place_single->slug;?>"><?= $place_single->name;?></a>
					<?= " "; ?>
				<?}?>
			</div>
		</div>

		<?the_content();?>

		<?php $subject = get_the_title(); ?>
		<a class="inscrever-button fancybox-inline" href="#contact-form" dataTitle ='<?= $subject . ' (ID ' . $post->ID; ?>' data-uk-toggle="{target:'#form'}">Inscrever-se</a>


		<div id="form" class="uk-hidden">
			<h3>Entre com os seus dados abaixo:</h3>
			<?$form = get_field('work_form', 'options');
			if ($form) {
				echo do_shortcode( $form );
			}

			endwhile; // End of the loop.
			?>

		</div>


	</div><!-- #primary -->
</div>

<script>

jQuery(document).ready(function($){

		$('.inscrever-button').click(function(){

			var title = $(this).attr('dataTitle');

			  $(".assunto").val(title);
				$('.assunto').prop( "disabled", false );


		});

});

</script>

<script type="text/javascript">

/* Máscaras ER */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}
function id( el ){
    return document.getElementById( el );
}
window.onload = function(){
    if (id('telefone')) {
      id('telefone').onkeypress = function(){
          mascara( this, mtel );
      }
    }

    if (id('celular')) {
      id('celular').onkeypress = function(){
        mascara( this, mtel );
      }
    }
}

</script>

<?php

get_footer();
