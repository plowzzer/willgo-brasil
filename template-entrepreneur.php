<?php
/**
*
*Template Name: Empreendedor
*Template texto: Usar como empreendedor
*
* @package one
*/

get_header(); ?>

<div class="entrepreneur">
  <div class="banner">
    <video autoplay id="bgvid" loop muted autoplay>
      <source src="<?=bloginfo('stylesheet_directory')?>/assets/videos/video1.mov" type="video/mp4">
    </video>

    <div class="content uk-hidden-small ">
      <?php the_field('texto'); ?>
      <a href="<?php the_field('link_ur'); ?>"><?php the_field('link_name'); ?></a>
    </div>
  </div>
  <div class="content-out uk-hidden-medium uk-hidden-large">
    <?php the_field('texto'); ?>
    <a href="<?php the_field('link_ur'); ?>"><?php the_field('link_name'); ?></a>
  </div>
  <section class="one" id="one">
    <div class="uk-container uk-container-center">
      <div class="uk-grid uk-grid-match" data-uk-grid-match="{target:'.uk-panel'}">
        <div class="uk-width-small-1-1 uk-width-medium-2-3">
          <h1><?php the_field('section-title'); ?></h1>
          <h4><?php the_field('section-subtitle'); ?></h4>
          <?php the_field('section-text'); ?>
          <span><a href="http://willgobrasil.com.br/novo/cadastro">Cadastre-se Já</a></span>
        </div>
        <div class="image uk-width-1-3 .uk-hidden-small">
          <?php $image = get_field('section-image');
          if( !empty($image) ): ?>
          	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <section class="video" id="video">
      <?php $image = get_field('image_to_call'); ?>
      <a href="<?php the_field('video-embed'); ?>" data-uk-lightbox><img class="image-call" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
      <h1>Como funciona, assista o vídeo</h1>
  </section>

  <section class="adv" id="adv">
    <div class="uk-container uk-container-center">
      <h1>Vantagens do Aplicativo</h1>
      <ul class="uk-width-small-1-1 uk-width-medium-3-4">
        <?php if( have_rows('vantagem') ):
            while ( have_rows('vantagem') ) : the_row();
                if( get_row_layout() == 'vantagem' ):
                  $image = get_sub_field('imagem'); ?>
                  <li class="uk-grid">
                    <div class="uk-width-1-4">
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>
                    <div class="uk-width-3-4">
                    	<h4><?php the_sub_field('titulo');?> </h4>
                      <?php the_sub_field('texto');?>
                    </div>
                  </li>
                <?php endif;
            endwhile;
        endif; ?>
      </ul>
      <div class="uk-width-1-4 uk-hidden-small"> </div>
    </div>
  </section>

  <section class="features" id="features">
    <div class="uk-container uk-container-center">
      <div class="uk-grid uk-grid-match" data-uk-grid-match="{target:'.uk-panel'}">
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <div class="uk-slidenav-position" data-uk-slideshow>
            <?php if( have_rows('feature') ): ?>
              <ul class="uk-slideshow">
                  <?php while ( have_rows('feature') ) : the_row(); ?>
                      <li>
                        <h1><?php the_sub_field('titulo'); ?></h1>
                        <?php the_sub_field('texto'); ?>
                        <?php $image = get_sub_field('image'); ?>
                        <p class="image_field"><img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" /></p>
                      </li>
                  <?php endwhile; ?>
              </ul>
              <?php endif; ?>
              <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
              <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
          </div>
        </div>
        <div class="uk-hidden-small uk-width-1-2">
          <img class="mao" src="<?=bloginfo('stylesheet_directory')?>/assets/img/mkp_willgo_mao.png" alt="" />
        </div>
      </div>
    </div>
  </section>

  <section class="ja-call" id="ja-call">
    <div class="uk-grid uk-grid-collapse">
      <div class="woman uk-width-1-2 uk-width-medium-1-2 uk-width-large-1-3 uk-hidden-small">
        <img class="back" src="<?=bloginfo('stylesheet_directory')?>/assets/img/mulher.jpg" alt="" />
        <?php $image4 = get_field('ja-imagem-4');
        if( !empty($image4) ): ?>
          <img class="icone" src="<?php echo $image4['url']; ?>" alt="<?php echo $image4['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="text_place uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3">
        <h1>WillGo Já</h1>
        <?php the_field('ja-texto'); ?>
        <span><a class="button-black center" href="<?php the_field('ja-url'); ?>">Saiba Mais</a></span>
        <div class="icons">
          <?php
          $image1 = get_field('ja-imagem-1');
          $image2 = get_field('ja-imagem-2');
          $image3 = get_field('ja-imagem-3');
          if( !empty($image1) ): ?>
            <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
            <img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
            <img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
          <?php endif; ?>
          </div>
        </div>
        <div class="moto uk-width-1-3 uk-hidden-small uk-hidden-medium">
          <img class="back" src="<?=bloginfo('stylesheet_directory')?>/assets/img/moto.jpg" alt="" />
          <?php $image5 = get_field('ja-imagem-5');
          if( !empty($image5) ): ?>
            <img class="ico" src="<?php echo $image5['url']; ?>" alt="<?php echo $image5['alt']; ?>" />
          <?php endif; ?>
        </div>
      </div>
  </section>

  <section class="plans" id="planos">
    <h1>Entenda como funciona a tarifação WillGo:</h1>
    <div class="back">
      <ul class="menu" data-uk-switcher="{connect:'#plans'}">
        <?php if( have_rows('veiculo') ):
          while ( have_rows('veiculo') ) : the_row();?>
            <li>
              <a href="">
                <?php $image = get_sub_field('icone'); ?>
                <img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" /><br>
                <?php the_sub_field('categoria_de_veiculos'); ?>
              </a>
            </li>
          <?php endwhile; ?>
      </ul>
    </div>

    <div class="uk-container uk-container-center">
      <ul id="plans" class="uk-switcher">
        <?php while ( have_rows('veiculo') ) : the_row();?>
          <li class="uk-grid" style="margin-top: 35px;">
            <div class="uk-width-small-1-1 uk-width-medium-1-3">
              <?php $image = get_sub_field('image'); ?>
              <img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" />
            </div>
            <div class="uk-width-small-1-1 uk-width-medium-2-3">
              <?php the_sub_field('texto'); ?>
              <table>
                <tr class="top">
                  <th>Tarifa Mínima</th>
                  <th>Tarifa por KM</th>
                  <th>Tarifa por Min</th>
                </tr>
                <tr>
                  <td><?php the_sub_field('tarifa_minima'); ?></td>
                  <td><?php the_sub_field('tarifa_por_km'); ?></td>
                  <td><?php the_sub_field('tarifa_por_min'); ?></td>
                </tr>
              </table>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
    </div>
  </section>


  <!-- <section id="promo">
    <div class="uk-container uk-container-center">
      <h2>Saiba como ganhar 5% de desconto em todas as suas corridas durante 1 ano.</h2>
      <div class="uk-grid">
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <img src="<?=bloginfo('stylesheet_directory')?>/assets/img/premium.png" alt="Premium" />
        </div>
        <div class="uk-width-small-1-1 uk-width-medium-1-2 info">
          <p><strong>Faça download do APP</strong></p>
          <div class="apps_downloads">
            <img class="uk-width-1-2" src="<?=bloginfo('stylesheet_directory')?>/assets/img/andorid.png" alt="Google Play Strore" />
            <img class="uk-width-1-2" src="<?=bloginfo('stylesheet_directory')?>/assets/img/apple.png" alt="App Store da Apple" />
          </div>
          <p>até o dia</p>
          <div class="save-the-date">
            <h2>01</h2>
            <h2>05</h2>
            <h2>16</h2>
          </div>
        </div>
      </div>
    </div>
    <h5>5% de desconto em todas as suas corridas por 1 ano.</h5>
    <p>Aguarde em seu e-mail o código de Cliente Premium Grátis!</p>
  </section> -->
</div>


<!-- Código do Google para tag de remarketing -->
<!--------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 878002129;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/878002129/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php get_footer(); ?>
