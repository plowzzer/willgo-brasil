<?php
/**
*
*Template Name: Usuario
*Template texto: Usar como usuario
*
* @package one
*/

get_header(); ?>

<div class="user">
  <div class="banner">
    <video autoplay id="bgvid" loop muted autoplay>
      <source src="<?=bloginfo('stylesheet_directory')?>/assets/videos/video2.mov" type="video/mp4">
    </video>
    <div class="content">
      <?php the_field('texto'); ?>
      <?php if ( the_field('link_name') ): ?>
        <a href="<?php the_field('link_ur'); ?>"><?php the_field('link_name'); ?></a>
      <?php endif ?>
    </div>
  </div>

  <section class="one" id="one">
    <div class="uk-container uk-container-center">
      <div class="uk-grid uk-grid-match" data-uk-grid-match="{target:'.uk-panel'}">
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <h1><?php the_field('section-title'); ?></h1>
          <!-- <h4><?php the_field('section-subtitle'); ?></h4> -->
          <?php the_field('section-text'); ?>
          <span><a class="button-black right" href="https://play.google.com/store/apps/details?id=com.willgo">Baixe já o app</a></span>
        </div>
        <div class="uk-width-1-2 uk-hidden-small"></div>
      </div>
    </div>
    <img class="uk-hidden-small" id="cell" src="<?=bloginfo('stylesheet_directory')?>/assets/img/one-user-bg.png" alt="" />

  </section>

  <section class="adv" id="adv">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-hidden-small image_place uk-width-1-2">
          <?php $image = get_field('imagem-user-favoritar');
          if( !empty($image) ): ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <h1>Possibilidade de favoritar seus motoristas preferidos</h1>
          <?php the_field('user-favoritar'); ?>
          <span><a class="button-black right" href="https://play.google.com/store/apps/details?id=com.willgo">Baixe já o app</a></span>
        </div>
      </div>
    </div>
  </section>

  <section class="objects" id="objects">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <h1>Plataforma integrada entre passageiros e objetos</h1>
          <?php the_field('user-objects'); ?>
          <span><a class="button-black right" href="https://play.google.com/store/apps/details?id=com.willgo">Baixe já o app</a></span>
        </div>
        <div class="uk-hidden-small image_place uk-width-1-2">
          <?php $image = get_field('imagem-user-objects');
          if( !empty($image) ): ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <section class="video" id="video">
      <?php $image = get_field('image_to_call'); ?>
      <a href="<?php the_field('video-embed'); ?>" data-uk-lightbox><img class="image-call" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
      <h1>Como funciona, assista o vídeo</h1>
  </section>

  <section class="cancel" id="cancel">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-hidden-small image_place uk-width-1-2">
          <?php $image = get_field('imaged-user-cancel');
          if( !empty($image) ): ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <h1>Agendamento com até 48hs de antecedência.</h1>
          <?php the_field('user-cancel'); ?>
          <span><a class="button-black right" href="https://play.google.com/store/apps/details?id=com.willgo">Baixe já o app</a></span>
        </div>
      </div>
    </div>
  </section>

  <section class="varieties" id="varieties">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <h1>Grande variedades de veículos</h1>
          <?php the_field('user-varieties'); ?>
          <span><a class="button-black right" href="https://play.google.com/store/apps/details?id=com.willgo">Baixe já o app</a></span>
        </div>
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <?php $image = get_field('imagem-users-varieties');
          if( !empty($image) ): ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
      </div>
    </div>
  </section>

  <section class="promo" id="promo">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="image_place uk-width-small-1-1 uk-width-medium-1-2">
          <?php $image = get_field('imagem-users-promo');
          if( !empty($image) ): ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
          <?php endif; ?>
        </div>
        <div class="uk-width-small-1-1 uk-width-medium-1-2">
          <h1>Conta Premium com 5% de desconto em todas as corridas durante 1 ano</h1>
          <?php the_field('user-promo'); ?>
          <span><a class="button-black right" href="https://play.google.com/store/apps/details?id=com.willgo">Baixe já o app</a></span>
        </div>
      </div>
    </div>
  </section>

  <section class="ja-call" id="ja-call">
    <div class="uk-grid uk-grid-collapse">
      <div class="woman uk-width-1-2 uk-width-medium-1-2 uk-width-large-1-3 uk-hidden-small">
        <img class="back" src="<?=bloginfo('stylesheet_directory')?>/assets/img/mulher.jpg" alt="" />
        <?php $image4 = get_field('ja-imagem-4');
        if( !empty($image4) ): ?>
          <img class="icone" src="<?php echo $image4['url']; ?>" alt="<?php echo $image4['alt']; ?>" />
        <?php endif; ?>
      </div>
      <div class="text_place uk-width-1-1 uk-width-medium-1-2 uk-width-large-1-3">
        <h1>WillGo Já</h1>
        <?php the_field('ja-texto'); ?>
        <span><a class="button-black center" href="<?php the_field('ja-url'); ?>">Saiba Mais</a></span>
        <div class="icons">
          <?php
          $image1 = get_field('ja-imagem-1');
          $image2 = get_field('ja-imagem-2');
          $image3 = get_field('ja-imagem-3');
          if( !empty($image1) ): ?>
            <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
            <img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
            <img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
          <?php endif; ?>
          </div>
        </div>
        <div class="moto uk-width-1-3 uk-hidden-small uk-hidden-medium">
          <img class="back" src="<?=bloginfo('stylesheet_directory')?>/assets/img/moto.jpg" alt="" />
          <?php $image5 = get_field('ja-imagem-5');
          if( !empty($image5) ): ?>
            <img class="ico" src="<?php echo $image5['url']; ?>" alt="<?php echo $image5['alt']; ?>" />
          <?php endif; ?>
        </div>
      </div>
  </section>

  <section class="plans" id="planos">
    <h1>Entenda como funciona a tarifação WillGo:</h1>
    <div class="back">
      <ul class="menu" data-uk-switcher="{connect:'#plans'}">
        <?php if( have_rows('veiculo') ):
          while ( have_rows('veiculo') ) : the_row();?>
            <li>
              <a href="">
                <?php $image = get_sub_field('icone'); ?>
                <img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" /><br>
                <?php the_sub_field('categoria_de_veiculos'); ?>
              </a>
            </li>
          <?php endwhile; ?>
      </ul>
    </div>
    <div class="uk-container uk-container-center">
      <ul id="plans" class="uk-switcher">
        <?php while ( have_rows('veiculo') ) : the_row();?>
          <li class="uk-grid" style="margin-top: 35px;">
            <div class="uk-width-small-1-1 uk-width-medium-1-3">
              <?php $image = get_sub_field('image'); ?>
              <img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" />
            </div>
            <div class="uk-width-small-1-1 uk-width-medium-2-3">
              <?php the_sub_field('texto'); ?>
              <table>
                <tr class="top">
                  <th>Tarifa Mínima</th>
                  <th>Tarifa por KM</th>
                  <th>Tarifa por Min</th>
                </tr>
                <tr>
                  <td><?php the_sub_field('tarifa_minima'); ?></td>
                  <td><?php the_sub_field('tarifa_por_km'); ?></td>
                  <td><?php the_sub_field('tarifa_por_min'); ?></td>
                </tr>
              </table>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
    </div>
  </section>

</div>


<?php get_footer(); ?>
