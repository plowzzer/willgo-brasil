<?php
/**
*
*Template Name: Cargo
*Template texto: Usar como pagina WillGo Cargo
*
* @package one
*/

get_header(); ?>

<div class="cargo">
  <?$background = get_field('banner');?>
  <div class="banner" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
    <div class="content uk-hidden-small ">
      <?php the_field('texto_banner'); ?>
    </div>
  </div>
  <div class="content-out uk-hidden-medium uk-hidden-large">
    <?php the_field('texto_banner'); ?>
  </div>

  <div id="about" class="uk-container uk-container-center">
    <div class="uk-grid sobre">
      <div class="uk-width-1-2 uk-hidden-small" style="text-align:center;">
        <?$sobre = get_field('sobre_imagem');?>
        <img src="<?php echo $sobre['url']; ?>" alt="<?php echo $sobre['alt']; ?>" />
      </div>
      <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('sobre_texto'); ?>
      </div>
    </div>
  </div>

  <div id="beneficios" class="uk-container uk-container-center">
    <h1>Beneficios WillGo Cargo</h1>
    <ul class="uk-grid beneficios">
      <? if( have_rows('beneficios') ):
        while ( have_rows('beneficios') ) : the_row();?>
          <li class="uk-width-1-1 uk-width-medium-1-4">
            <?$image = get_sub_field('imagem');?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            <h3><?the_sub_field('titulo');?></h3>
            <?the_sub_field('texto');?>
          </li>
        <?endwhile;
      endif;?>
    </ul>
  </div>

  <?$background = get_field('para_voce_bg');?>
  <div id="para_voce" class="paravoce" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <h1>Para Você</h1>
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-2">
          <?the_field('para_voce');?>
        </div>
      </div>

    </div>
  </div>

  <?$background = get_field('para_sua_empresa_bg');?>
  <div id="sua_empresa" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <div class="uk-grid">
        <div class="uk-width-1-1 uk-width-medium-1-2 uk-push-1-2">
          <h1>Para sua Empresa</h1>
          <?the_field('para_sua_empresa');?>
          <p style="text-align: center;">
            <span><a class="cadastro-btn" href="https://www.willgobrasil.com.br/novo/site-pack-empresa-cadastro_empresa">Cadastre sua empresa</a></span>
          </p>
        </div>
      </div>
    </div>
  </div>

  <?$background = get_field('banner_delivery');?>
  <div class="banner-delivery" id="delivery" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
    <div class="content uk-hidden-small ">
      <?php the_field('texto_banner_delivery'); ?>
    </div>
  </div>
  <div class="content-out-delivery uk-hidden-medium uk-hidden-large">
    <?php the_field('texto_banner_delivery'); ?>
  </div>


  <section class="plans" id="planos">
    <div class="back">
      <ul class="menu" data-uk-switcher="{connect:'#plans'}">
        <?php if( have_rows('veiculos') ):
          while ( have_rows('veiculos') ) : the_row();?>
            <li>
              <a href="">
                <?php $image = get_sub_field('icone'); ?>
                <img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" /><br>
                <?php the_sub_field('categoria_de_veiculos'); ?><br>
                <small><?php the_sub_field('ate_peso');?></small>
              </a>
            </li>
          <?php endwhile; ?>
      </ul>
    </div>

    <div class="uk-container uk-container-center">
      <ul id="plans" class="uk-switcher">
        <?php while ( have_rows('veiculos') ) : the_row();?>
          <li class="uk-grid" style="margin-top: 35px;">
            <div class="uk-width-small-1-1 uk-width-medium-1-3">
              <?php $image = get_sub_field('imagem'); ?>
              <img src="<?php echo $image['url']; ?>" class="feature_image" alt="<?php echo $image['alt']; ?>" />
            </div>
            <div class="uk-width-small-1-1 uk-width-medium-2-3">
              <p style="text-align:center;"><?php the_sub_field('texto'); ?></p>
              <ul class="tax">
                <li>
                  <span>
                    <h4>Tarifa Mínima</h4>
                    <p><?php the_sub_field('tarifa_minima'); ?></p>
                  </span>
                </li>
                <li>
                  <span>
                    <h4>Tarifa por KM</h4>
                    <p><?php the_sub_field('tarifa_por_km'); ?></p>
                  </span>
                </li>
                <li>
                  <span>
                    <h4>Tarifa por min.</h4>
                    <p><?php the_sub_field('tarifa_por_min'); ?></p>
                  </span>
                </li>

              </ul>
            </div>
          </li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
    </div>
  </section>



  <div class="formulario" id="contact">
    <div class="uk-container uk-container-center">
        <?php $formulario = get_field('formulario_de_contato');
        echo do_shortcode($formulario);?>
      </div>
      <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div>

</div>


<?php get_footer(); ?>
