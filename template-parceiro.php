4<?php
/**
*
*Template Name: Parceiros
*Template texto: Usar como pagina WillGo Parceiros e apoio.
*
* @package one
*/

get_header(); ?>

<div class="parceiros">
  <h1 class="title">Apoio e Parcerias</h1>
  <div class="uk-container uk-container-center into">
    <?php while ( have_posts() ) : the_post();
      the_content();
    endwhile; ?>
  </div>
  <div class="hands">
    <img src="<?=bloginfo('stylesheet_directory')?>/assets/img/parceiro/hands.png" alt="Parceiros" />
  </div>
  <div class="about" id="about">
    <div class="uk-container uk-container-center">
      <h1>O que é</h1>
      <? the_field('o_que_e'); ?>
    </div>
  </div>
  <div class="list">
    <h1>Empresas Parceiras</h1>
    <div class="uk-container uk-container-center">
      <?  $args = array (
      	'post_type'              => array( 'parceiros' ),
      );
      $parceiros = new WP_Query( $args );?>

      <? if ( $parceiros->have_posts() ) {?>
        <!-- categorias MENU-->
        <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#categories'}">
          <li><a>Todos</a></li>
          <?php $categories = get_categories( array(
              'taxonomy' => 'area_de_atuacao',
              'orderby' => 'name',
              'parent'  => 0
          ) );
          foreach($categories as $category) { ?>
            <li><a><?php echo $category->cat_name; ?></a></li>
          <?php } ?>
        </ul>


        <!-- Categorias Resto -->
        <ul id="categories" class="uk-switcher">
          <!-- Para todos -->
          <li>
            <? while ( $parceiros->have_posts() ) {
              $parceiros->the_post();?>
              <div class="uk-grid single" data-uk-grid-match>
                <div class="uk-width-1-3 img-field">
                  <? the_post_thumbnail(); ?>
                </div>
                <div class="uk-width-2-3 content">
                  <? the_title('<h3>','</h3>'); ?>
                  <? the_content(); ?>
                </div>
              </div>
            <? } ?>
          </li>


          <!-- Categorias solidas -->
          <?php $categories = get_categories( array(
              'taxonomy' => 'area_de_atuacao',
              'orderby' => 'name',
              'parent'  => 0
          ) );


          foreach($categories as $category) {
            $mycategory = $category->slug;


            $args2 = array (
              'post_type'              => array( 'parceiros' ),
              'tax_query' => array(
            		array(
            			'taxonomy' => 'area_de_atuacao',
                  'field' => 'slug',
                  'terms' => $mycategory
            		),
            	),
            );

            // The Query
            $inside = new WP_Query( $args2 );
            // The Loop
            if ( $inside->have_posts() ) {?>
              <li>
            	<? while ( $inside->have_posts() ) {
            		$inside->the_post(); ?>

                <div class="uk-grid single" data-uk-grid-match>
                  <div class="uk-width-1-3 img-field">
                    <? the_post_thumbnail(); ?>
                  </div>
                  <div class="uk-width-2-3 content">
                    <? the_title('<h3>','</h3>'); ?>
                    <? the_content(); ?>
                  </div>
                </div>
            	<? } ?>
            </li>
            <? } ?>
          <? } //foreach ?>
        </ul>
      <? } else {
      	// no posts found
      }
      // Restore original Post Data
      wp_reset_postdata();?>
    </div>
  </div>

  <div class="formulario">
    <div class="uk-container uk-container-center">
        <?php $formulario = get_field('formulario_de_contato');
        echo do_shortcode($formulario);?>
      </div>
      <script type="text/javascript">
      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          id('telefone').onkeypress = function(){
              mascara( this, mtel );
          }
      }
      </script>
  </div>

</div>


<?php get_footer(); ?>
