<?php
/**
*
*Template Name: Franquia
*Template texto: Usar como pagina WillGo Franchising
*
* @package one
*/

get_header(); ?>

<div class="franquia">
  <?$background = get_field('banner');?>
  <div class="banner" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
    <div class="content uk-hidden-small ">
      <?php the_field('texto_banner'); ?>
    </div>
  </div>
  <div class="content-out uk-hidden-medium uk-hidden-large">
    <?php the_field('texto_banner'); ?>
  </div>

  <div id="about" class="uk-container uk-container-center">
    <div class="uk-grid sobre">
      <div class="uk-width-1-2 uk-hidden-small" style="text-align:center;">
        <?$sobre = get_field('sobre_imagem');?>
        <img src="<?php echo $sobre['url']; ?>" alt="<?php echo $sobre['alt']; ?>" />
      </div>
      <div class="uk-width-1-1 uk-width-medium-1-2">
          <?php the_field('sobre_texto'); ?>
      </div>
    </div>
  </div>

  <?$background = get_field('mercado_de_economia_bg');?>
  <div id="merc" class="mercado" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
    <div class="uk-container uk-container-center">
      <h1>O Mercado de Economia<br>Compartilhada no Brasil</h1>

      <ul class="uk-grid check">
        <? if( have_rows('mercado_de_economia') ):?>
          <?while ( have_rows('mercado_de_economia') ) : the_row();?>
            <li class="uk-width-1-1"><?the_sub_field('texto');?></li>
          <?endwhile;
        endif;?>
      </ul>
    </div>
  </div>

  <div id="concpt" class="uk-container uk-container-center conceito">
    <h1>Conceito da Franquia WillGo</h1>
    <ul class="uk-grid conceito">
      <? if( have_rows('conceito') ):
        while ( have_rows('conceito') ) : the_row();?>
          <li class="uk-width-1-1 uk-width-medium-1-3">
            <?$image = get_sub_field('imagem');?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            <?the_sub_field('texto');?>
          </li>
        <?endwhile;
      endif;?>
    </ul>
  </div>

  <?$background = get_field('suporte_bg');?>
  <div id="sos" class="suporte" style="background-image: url(<?php echo $background['url']; ?>);">
    <div class="uk-container uk-container-center">
      <h1>Suporte ao franqueado</h1>
      <ul class="uk-grid check">
        <? if( have_rows('suporte') ):
          while ( have_rows('suporte') ) : the_row();?>
            <li class="uk-width-1-1 uk-width-medium-1-2"><?the_sub_field('texto');?></li>
            <p class="uk-width-1-2 uk-hidden-small"></p>
          <?endwhile;
        endif;?>
      </ul>
    </div>
  </div>

  <div id="atuacao" class="diferenciais">
    <h1>Diferenciais</h1>
    <div class="uk-container uk-container-center">
      <ul class="uk-grid check">
        <? if( have_rows('diferenciais') ):
          while ( have_rows('diferenciais') ) : the_row();?>
            <li class="uk-width-1-1 uk-width-medium-1-2"><?the_sub_field('texto');?></li>
          <?endwhile;
        endif;?>
      </ul>
    </div>
  </div>

  <div id="noticias" class="uk-container uk-container-center news">
    <h1>WillGo na Midia</h1>
    <ul class="uk-grid">
      <? if( have_rows('noticias') ):
        while ( have_rows('noticias') ) : the_row();?>
          <li class="uk-width-1-1 uk-width-medium-1-3">
            <?$image = get_sub_field('imagem');?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            <h3><?the_sub_field('titulo_noticia');?></h3>
            <p><?the_sub_field('descricao');?></p>
            <a class="btn" href="<?the_sub_field('url_noticia');?>">Saiba Mais</a>
          </li>
        <?endwhile;
      endif;?>
    </ul>
  </div>

  <?$background = get_field('imagem_do_raio');?>
  <div class="banner-bottom" id="raio" style="background-image: url(<?php echo $background['url']; ?>); background-size: cover;">
  </div>



  <div class="formulario" id="contact">
    <div class="uk-container uk-container-center">
        <?php $formulario = get_field('formulario_de_contato');
        echo do_shortcode($formulario);?>
      </div>
      <script type="text/javascript">

      /* Máscaras ER */
      function mascara(o,f){
          v_obj=o
          v_fun=f
          setTimeout("execmascara()",1)
      }
      function execmascara(){
          v_obj.value=v_fun(v_obj.value)
      }
      function mtel(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
          v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function mdinheiro(v){
          v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
          v=v.replace(/(\d)(\d{2})$/,"$1.$2");    //Coloca hífen entre o quarto e o quinto dígitos
          return v;
      }
      function id( el ){
          return document.getElementById( el );
      }
      window.onload = function(){
          if (id('telefone')) {
            id('telefone').onkeypress = function(){
                mascara( this, mtel );
            }
          }

          if (id('celular')) {
            id('celular').onkeypress = function(){
              mascara( this, mtel );
            }
          }

          if (id('valor')) {
            id('valor').onkeypress = function(){
              mascara( this, mdinheiro );
            }
          }

          if (id('valor2')) {
            id('valor2').onkeypress = function(){
              mascara( this, mdinheiro );
            }
          }
      }

      </script>
  </div>

</div>


<?php get_footer(); ?>
