<?php
/**
*
*Template Name: Home-Splash
*Template texto: Usar como empreendedor
*
* @package one
*/

get_header(); ?>

<div class="home-page">
	<a href="empreendedor" id="choice-entrepreneur">
		<img class="background" src="<?=bloginfo('stylesheet_directory')?>/assets/img/splash/entrepreneur-bg.png" alt="" />
    <div class="block">
      <p class="text">
        Acessar como<br><strong>Empreendedor</strong>
      </p>
      <img class="ilustradedservice" src="<?=bloginfo('stylesheet_directory')?>/assets/img/splash/entrepreneur-app.png" alt="" />
    </div>
	</a>
	<a href="cliente" id="choice-user">
		<img class="background" src="<?=bloginfo('stylesheet_directory')?>/assets/img/splash/user-bg.png" alt="" />
    <div class="block">
      <p class="text">
  			Acessar como<br><strong>Cliente</strong>
  		</p>
  		<img class="ilustradedservice" src="<?=bloginfo('stylesheet_directory')?>/assets/img/splash/user-app.png" alt="" />
    </div>
	</a>
  <img id="logo" src="<?=bloginfo('stylesheet_directory')?>/assets/img/oval-logo.svg" alt="" />
</div>

<?php get_footer(); ?>
